#include "TreeElement.h"

/**
 * ������� ��������� �� ������ ���� �������� ��������
 * @return ��������� �� ������ ����
 */
TreeElement* TreeElement::getLeft() {
    return this->left;
}

/**
 * ������� ��������� �� ������� ���� �������� ��������
 * @return ��������� �� ������� ����
 */
TreeElement* TreeElement::getRight() {
    return this->right;
}

/**
 * ������� �������� �������� ��������
 * @return �������� �������� ��������
 */
char TreeElement::getData() {
    return this->data;
}

/**
 * ��������� �������� �������� ����� �����
 * @param elem ��������� �� ������� �������
 */
void TreeElement::setLeft(TreeElement* elem) {
    this->left = elem;
}

/**
 * ��������� �������� �������� ������ �����
 * @param elem ��������� �� ������� �������
 */
void TreeElement::setRight(TreeElement* elem) {
    this->right = elem;
}

/**
 * ��������� �������� � ������� ������� ������
 * @param data ��������
 */
void TreeElement::setData(char data) {
    this->data = data;
}
