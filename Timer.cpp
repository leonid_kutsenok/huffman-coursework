#include "Timer.h"

/**
 * ������ ������� �������
 */
void Timer::start() {
    this->time_start = high_resolution_clock::now();
}

/**
 * ����� ������� �������
 * @return ���������� ���������� ��������� � ���������� ������� �������
 */
double Timer::stop() {
    return duration_cast<microseconds>(high_resolution_clock::now() - this->time_start).count() / 1000.0;
}