#ifndef KURSACH_ENCODER_H
#define KURSACH_ENCODER_H

#include <map>
#include <sstream>
#include <iostream>
#include "TreeElement.h"
#include "Text.h"

using namespace std;

class Encoder {
public:
    explicit Encoder(string& input) : current_input(const_cast<char*>(input.c_str())) {}

    void encode(TreeElement* head, string& encoded);

    map<char, string> table;

    void printEncodingTable();

private:
    void formEncodingTable(TreeElement* head, string route);

    char* current_input;

};


#endif //KURSACH_ENCODER_H
