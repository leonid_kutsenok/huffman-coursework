#ifndef KURSACH_CODING_H
#define KURSACH_CODING_H

#include "Tree.h"
#include "Distributor.h"
#include <map>

class Coding {
public:
    int counter = 0;

    Tree* getTree() const;

    explicit Coding(map<TreeElement*, int> distribution) : distribution(distribution), tree(nullptr) {}

    virtual void formTree() = 0;

protected:
    Tree* tree;

    map<TreeElement*, int> distribution;
};


#endif //KURSACH_CODING_H
