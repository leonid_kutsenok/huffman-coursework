#include "Encoder.h"
#include "File.h"

/**
 * ����������� ��������� ������
 * @param head - ��������� �� ������� ������
 * @param encoded - ������ �� ������ ��� ������ ��������������� ������
 */
void Encoder::encode(TreeElement* head, string& encoded) {
    string route;
    //���� � ������ ����������� ���� ������, �� � ������� ����� ������ ���� ���������
    if(head->getLeft() == nullptr && head->getRight() == nullptr) {
        this->table[head->getData()] = '0';
    } else { //�����, �������� ��� �������
        this->formEncodingTable(head, route);
    }
    this->printEncodingTable(); //������� ��������� ������� � ����
    stringstream stream; //����� ��� ������ �������������� ������
    do { //�������� ��� ������� �������� ������
        stream << this->table[Text::getCurrentSymbol(current_input)];
    } while(Text::moveToNext(current_input));
    encoded = stream.str(); //��������� ������ �� ������ � ������
}

/**
 * ������������ ������� ���������
 * @param current - ��������� �� ������� ���� ������
 * @param route - ���� � �������� ���� ������
 */
void Encoder::formEncodingTable(TreeElement* current, string route) {
    //���� ��� ����, �� ���������� ���� � ���� � �������
    if(current->getLeft() == nullptr && current->getRight() == nullptr) {
        this->table[current->getData()] = route;
        return;
    }
    this->formEncodingTable(current->getLeft(), route + '0');
    this->formEncodingTable(current->getRight(), route + '1');
}

void Encoder::printEncodingTable() {
    stringstream coding;
    coding << "���� ��������:" << endl;
    for(auto elem : table) //��� ������� �������� ������� ������� ��������������� ��� ���������
    {
        coding <<"\""<< elem.first << "\"" << " - " << elem.second << endl;
    }
    string out = coding.str();
    File::writeCode(out);
}