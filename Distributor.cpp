#include <iostream>
#include "Distributor.h"

/**
 * Формирование таблицы частот повторений символов в исходном тексте
 * @param text - исходный текст
 */
void Distributor::formDistribution(string& text) {
    const char* current_input = text.c_str(); //переводим string в char* для прохода по каждому символу
    while(*current_input != '\0') { //пока еще есть символы во входной строке
        if(distribution.find(*current_input) == distribution.end()) {
            distribution[*current_input] = 1; //если данного ключа нет в массиве, то создаем
        } else {
            ++distribution[*current_input]; //если есть - то инкриментируем
        }
        current_input++;
    }
}


/**
 * Формирование распределения пар "узел" - "количество повторения символа в тексте" по уже сформированному
 * распределению частоты повторений
 * @return - сформированное распределение
 */
map<TreeElement*, int> Distributor::formElemDistribution() {
    map<TreeElement*, int> tree_distribution;
    TreeElement* temp;
    //для каждого элемента существующего распределения создаем лист и переносим этот элемент в новое распределение
    for(pair<char, int> elem : distribution) {
        temp = new TreeElement(elem.first);
        tree_distribution[temp] = elem.second;
    }
    return tree_distribution;
}