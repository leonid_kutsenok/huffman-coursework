#include "Text.h"

/**
 * ����������� ��������� �� ��������� ������
 * @param current_input ������ �� ��������� �� ������� ������
 * @return ��������� ��������� ����������� ���������
 */
bool Text::moveToNext(char*& current_input) {
    (current_input)++;
    if(*(current_input) == '\0') {
        return false;
    }
    return true;
}
 /**
  * ������� �������� ������� �� ���������
  * @param current_input ��������� �� ������� ������
  * @return ������� ������
  */
char Text::getCurrentSymbol(char* current_input) {
    return *(current_input);
}

/**
 * ������� ������� ����� �������������� ������ � ������
 * @param encoded ������ �� ������ � �������������� �������
 * @return ������ ��������������� ������ � ������
 */
int Text::getByteLength(string& encoded) {
    return (int)ceil(encoded.length() / 8.0);
}