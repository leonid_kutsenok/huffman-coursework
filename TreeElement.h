#ifndef KURSACH_TREEELEMENT_H
#define KURSACH_TREEELEMENT_H


class TreeElement {
private:
    char data;
    TreeElement* left;
    TreeElement* right;

public:
    TreeElement* getLeft();

    TreeElement* getRight();

    char getData();

    void setLeft(TreeElement* elem);

    void setRight(TreeElement* elem);

    void setData(char data);

    explicit TreeElement(char data = '\\') : data(data), left(nullptr), right(nullptr) {}
};

#endif //KURSACH_TREEELEMENT_H
