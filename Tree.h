#ifndef KURSACH_TREE_H
#define KURSACH_TREE_H
#include "TreeElement.h"

class Tree {
private:
    TreeElement* head;

public:
    TreeElement* getHead();

    int getNodesCount();

    int countNodes(TreeElement* current);

    explicit Tree(TreeElement* head) : head(head) {}
};


#endif //KURSACH_TREE_H
