#ifndef KURSACH_SHENNONFANO_H
#define KURSACH_SHENNONFANO_H

#include "Tree.h"
#include "Coding.h"
#include <map>


using namespace std;

class ShannonFano : public Coding {
public:

    void formTreeElement(TreeElement* current, vector<pair<TreeElement*, int>> current_elements);

    explicit ShannonFano(map<TreeElement*, int> distribution) : Coding(distribution) {}

    void formTree() override;

    static bool mapDiffCheck(const vector<pair<TreeElement*, int>> elements_left,
                             const vector<pair<TreeElement*, int>> elements_right);
};


#endif //KURSACH_SHENNONFANO_H
