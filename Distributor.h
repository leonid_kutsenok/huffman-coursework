#ifndef KURSACH_DISTRIBUTOR_H
#define KURSACH_DISTRIBUTOR_H

#include "TreeElement.h"
#include <map>
#include <vector>
#include <algorithm>
#include <string>

using namespace std;

class Distributor {
private:
    map<char, int> distribution;
public:
    void formDistribution(string& text);

    map<TreeElement*, int> formElemDistribution();

};


#endif //KURSACH_DISTRIBUTOR_H
