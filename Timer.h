#ifndef KURSACH_TIMER_H
#define KURSACH_TIMER_H

#include <chrono>

using namespace std::chrono;

class Timer {
private:
    high_resolution_clock::time_point time_start;
public:
    void start();
    double stop();
};


#endif //KURSACH_TIMER_H
