#include "ShannonFano.h"

/**
 * ������������ ������
 */
void ShannonFano::formTree() {
    this->tree = new Tree(new TreeElement);
    vector<pair<TreeElement*, int>> elements(this->distribution.size());
    std::copy(this->distribution.begin(), this->distribution.end(), elements.begin());
    //��������� �������� ������������� �� �������� ��� ���������� ������ ���������
    sort(elements.begin(), elements.end(),
         [](pair<TreeElement*, int> const& a, pair<TreeElement*, int> const& b) {
             if(a.second == b.second) {
                 return (unsigned int)a.first->getData() < (unsigned int)b.first->getData();
             }
             return a.second > b.second;
         });
    this->formTreeElement(this->tree->getHead(), elements);
}

/**
 * ����������� ������� ��� ������������� ��������� �� ���� �������� � ������������ � ������������ �������
 * (�������� ����� ������ ��������� � ����� � ������ �������� ������ ���� ����������)
 * @param current ��������� �� ������� ������� ������
 * @param current_elements ������ ��� <��������� �� ������� ������, ����� ����������>
 */
void ShannonFano::formTreeElement(TreeElement* current, vector<pair<TreeElement*, int>> current_elements) {
    //���� ������� ������� �������� ������, �� ���������� ��� ������ � ������� �������������
    if(current_elements.size() == 1) {
        current->setData(current_elements.begin()->first->getData());
        return;
    }
    //���� � ������������� ��� ��������, �� ���������� ����� �����, � ���������� - ������
    if(current_elements.size() == 2) {
        if(current_elements.begin()->second < current_elements.rbegin()->second) {
            current->setLeft(current_elements.begin()->first);
            current->setRight(current_elements.rbegin()->first);
        } else {
            current->setLeft(current_elements.rbegin()->first);
            current->setRight(current_elements.begin()->first);
        }
        return;
    }
    //���� � ������������� ������ ���� ���������
    vector<pair<TreeElement*, int>> elements_left(current_elements.size());
    vector<pair<TreeElement*, int>> elements_right(current_elements.size());

    bool stop_check = false;
    int i = 0;
    //��������� ������� ������������� �� ��� ����� �� ���������� �������� ��� ����� � ������ ������������
    std::partition_copy(current_elements.begin(), current_elements.end(), elements_left.begin(), elements_right.begin(),
                        [&stop = stop_check, &count = i, &left = elements_left, current_elements](
                                pair<TreeElement*, int> elem) {
                            if(stop) {
                                return false;
                            }
                            count++;
                            stop = ShannonFano::mapDiffCheck(left, current_elements);
                            return !stop;
                        });
    this->counter += i * current_elements.size();
    elements_left.resize(i - 1);
    elements_right.resize(current_elements.size() - i + 1);
    //������� ����� ������������� ����������� � ����� ���������, � ������� - � ������
    current->setLeft(new TreeElement);
    current->setRight(new TreeElement);
    this->formTreeElement(current->getRight(), elements_right);
    this->formTreeElement(current->getLeft(), elements_left);
}

/**
 * ������� ����� ��������� � ����� � ������ �������
 * @param elements_left ����� (�����) ������ ���������
 * @param elements_current ������� ������ ���������
 * @return ��������� ���������� ���������� �������
 */
bool ShannonFano::mapDiffCheck(const vector<pair<TreeElement*, int>> elements_left,
                               const vector<pair<TreeElement*, int>> elements_current) {
    int sum_left_current = 0;
    int sum_right_current = 0;
    int i = 0;
    //������� ����� ����� �����
    std::vector<pair<TreeElement*, int>>::const_iterator iter;
    for(iter = elements_left.begin(); iter != elements_left.end(); ++iter) {
        sum_left_current += iter->second;
        if(iter->second == 0) break;
        i++;
    }
    //������� ����� ������ �����
    for(int n = i; n < elements_current.size(); n++) {
        sum_right_current += elements_current[n].second;
    }
    //�������� ���� �� ������� ��������
    int diff_current = abs(sum_left_current - sum_right_current);
    //�������� ���� �� ��������� ��������
    int diff_next = abs(
            (sum_left_current + elements_current[i].second) - (sum_right_current - elements_current[i].second));
    //���� ������ �������� ������, ������ ��� ����������, ���������
    //���� ������, �� ����� ��� ���� ��������
    return diff_current <= diff_next;
}
