#include "Huffman.h"
#include <climits>

/**
 * ������������ ������
 */
void Huffman::formTree() {
    vector<pair<TreeElement*, int>> elements;
    for(pair<TreeElement*,int> elem : this->distribution) {
        elements.push_back(elem);
    }
    //��������� �������� ������������� �� �������� ��� ���������� ������ ���������
    sort(elements.begin(), elements.end(),
         [](pair<TreeElement*, int> const& a, pair<TreeElement*, int> const& b) {
             if(a.second == b.second) {
                 return (unsigned int)a.first->getData() > (unsigned int)b.first->getData();
             }
             return a.second < b.second;
         });
    pair<TreeElement*, int>* first_min;
    pair<TreeElement*, int>* second_min;
    TreeElement* temp;
    int sum = 0;
    while(elements.size() > 1) { //���� ��� ����� ����� ��� ����������� ��������
        this->getMin(elements, first_min, second_min);
        //���������� �� ����� � ������� �� ��������� �������������
        sum = first_min->second + second_min->second;
        auto it = std::remove_if(elements.begin(), elements.end(), [first_min, second_min] (pair<TreeElement*, int> elem) {
            return (elem.first == first_min->first || elem.first == second_min->first);
        });
        elements.erase(it, elements.end());
        temp = new TreeElement(); //������� ����� ������� ������
        temp->setLeft(first_min->first); //������� ������� ���� �����
        temp->setRight(second_min->first); //� ������� - ������

        it = std::find_if(elements.begin(), elements.end(), [sum, &counter = this->counter] (pair<TreeElement*, int> elem) {
            counter++;
            return elem.second > sum;
        });
        elements.insert(it, std::make_pair(temp,sum)); //��������� ��������� ���� � �������� �������������
        this->counter++;
    }
    this->tree = new Tree(elements.begin()->first);
}

/**
 * ����� ���� ���������� ��������� �������� �������������
 * @param distribution - ������� ������������� ��������
 * @param first_min - ������ �� ������ ����������� ����
 * @param second_min - ������ �� ������ ����������� ����
 */
void Huffman::getMin(vector<pair<TreeElement*, int>>& distribution, pair<TreeElement*, int>*& first_min,
                     pair<TreeElement*, int>*& second_min) {
    first_min = nullptr;
    second_min = nullptr;
    int min = INT_MAX;
    int min_prev = INT_MAX;
    //��� ������� �������� �������� ������������� ���� ��� ���������� ��������
    for(pair<TreeElement*, int> elem : distribution) {
        this->counter++;
        if(elem.second < min) {
            second_min = first_min;
            min_prev = min;
            first_min = new pair<TreeElement*,int> (elem.first, elem.second);
            min = elem.second;
        } else if(elem.second < min_prev) {
            second_min = new pair<TreeElement*,int> (elem.first, elem.second);
            min_prev = elem.second;
        }
    }
}