#ifndef KURSACH_INPUT_H
#define KURSACH_INPUT_H

#include <iostream>
#include "Coding.h"
#include "File.h"
#include "Generator.h"
#include "Huffman.h"
#include "ShannonFano.h"
#include <iostream>
#include <cstdio>
#include <limits>


using namespace std;

class Input {
private:
    int coding_type;

    int input_type;

    int generator_type;
public:
    void askCodingType();

    void askInputType();

    void askGeneratorType();

    string getInput();

    Coding* getCoding(Distributor* distributor);

    string getDistribution();

    int askGeneratorCharNum();


};


#endif //KURSACH_INPUT_H
