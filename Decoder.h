#ifndef KURSACH_DECODE_H
#define KURSACH_DECODE_H

#include <iostream>
#include "TreeElement.h"
#include "Text.h"

using namespace std;

class Decoder {
public:
    explicit Decoder(string& input) : current_input(const_cast<char*>(input.c_str())) {}

    void decode(TreeElement* head, string& decoded);

private:
    bool getDecodedSymbol(TreeElement* current, char* symb, string route, bool skip = false);

    char* current_input;
};


#endif //KURSACH_DECODE_H
