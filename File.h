#ifndef KURSACH_FILE_H
#define KURSACH_FILE_H

#include <fstream>
#include <iostream>
#include <sstream>

using namespace std;

class File {
public:
    static bool read(string& text);

    static bool writeText(string& text);

    static bool writeCode(string& text);
};

#endif //KURSACH_FILE_H
