#ifndef KURSACH_GENERATOR_H
#define KURSACH_GENERATOR_H

#include <iostream>
#include <ctime>
#include <sstream>
#include <random>
#include <vector>
#include <algorithm>


using namespace std;

class Generator {
public:
    static string uniformDistribution(int n);
    static string randomDistribution(int n);
};


#endif //KURSACH_GENERATOR_H
