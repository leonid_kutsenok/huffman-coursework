#ifndef KURSACH_HUFFMAN_H
#define KURSACH_HUFFMAN_H

#include "Tree.h"
#include "Distributor.h"
#include "Coding.h"
#include <map>

using namespace std;

class Huffman : public Coding {
public:

    void formTree() override;

    explicit Huffman(map<TreeElement*, int> distribution) : Coding(distribution) {}

    void getMin(vector<pair<TreeElement*, int>>& distribution, pair<TreeElement*, int>*& first_min,
                pair<TreeElement*, int>*& second_min);
};

#endif //KURSACH_HUFFMAN_H
