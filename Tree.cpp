#include "Tree.h"

/**
 * ������� ��������� �� ������� ������
 * @return ��������� �� ������� ������
 */
TreeElement* Tree::getHead() {
    return this->head;
}

/**
 * ����� ������� ��� �������� ���������� ����� ������
 * @return ���������� ����� ������
 */
int Tree::getNodesCount() {
    return this->countNodes(this->head);
}

/**
 * ����������� ������� �������� ���������� ����� ������
 * @param current ��������� �� ������� ������� ������
 * @return ���������� ����� ������
 */
int Tree::countNodes(TreeElement* current) {
    if(current == nullptr) {
        return 0;
    }
    return this->countNodes(current->getLeft()) + this->countNodes(current->getRight()) + 1;
}