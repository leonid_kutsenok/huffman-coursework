#include "Generator.h"

/**
 * ��������� ������ c ����������� �������������� ��������
 * @param n - ���������� ��������, ������� ���������� �������������
 * @return - ��������������� �����
 */
string Generator::uniformDistribution(int n) {
    string text;
    std::default_random_engine generator; //����������� ������������
    std::uniform_int_distribution<char> distribution('0', 'z'); //������� ����� �������������� � ���������� '0'-'z'

    for(int i = 0; i < n; ++i) { //���������� n �������� � ���������� �� � ������
        text += distribution(generator);
    }
    return text;
}
/**
 * ��������� ������ � ������������� �������������� ��������
 * @param n - ���������� ��������, ������� ���������� �������������
 * @return - ��������������� �����
 */
string Generator::randomDistribution(int n) {
    //������ �������� ��������
    static vector<char> chars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G',
                                 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X',
                                 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o',
                                 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z'};
    vector<double> weights;
    std::random_device rd;
    std::mt19937 gen(rd());
    double upper = 1.0;
    for(auto a : chars) { //��� ������� ������� ���������� ��� ��������� ��� � ������
        std::uniform_real_distribution<> dis(0.0, upper);
        double num = dis(gen);
        if(num > 0.1) {
            num *= 0.1;
        }
        upper -= num;
        weights.push_back(num);
    }

    string text;
    std::default_random_engine generator;
    std::discrete_distribution<char> distribution(weights.begin(), weights.end());
    for(int i = 0; i < n; i++) { //���������� n �������� �� ������ ���������� ������������� �����
        text += chars[distribution(generator)];
    }
    return text;
}