#include "Input.h"

using namespace std;

/**
 * ������ � ������������ ���� ����� ��������� ������
 */
void Input::askInputType() {
    bool error = true;
    while(error) {
        cin.clear();
        cout << "��� ����� ������ � ������� ������� 1, �� ����� ������� 2, ����� ������������� ����� ������� 3: ";
        int input = 0;
        cin >> input;
        switch(input) {
            case 1:
                this->input_type = 1;
                error = false;
                break;
            case 2:
                this->input_type = 2;
                error = false;
                break;
            case 3:
                this->input_type = 3;
                error = false;
                break;
            default:
                cout << "�������� ����!" << endl;
                break;
        }
    }
}

/**
 * ������ � ������������ ������������ ���� �����������
 */
void Input::askGeneratorType() {
    bool error = true;
    while(error) {
        cin.clear();
        cout << "��� ��������� ������ � ����������� �������������� �������� ������� 1, ������������� ������� 2: ";
        int input = 0;
        cin >> input;
        switch(input) {
            case 1:
                this->generator_type = 1;
                error = false;
                break;
            case 2:
                this->generator_type = 2;
                error = false;
                break;

            default:
                cout << "�������� ����!" << endl;
                break;
        }
    }
}

/**
 * ������ � ������������ ������������ ���� �����������
 */
void Input::askCodingType() {
    bool error = true;
    while(error) {
        cin.clear();
        cout << "��� ����������� � ������������� ������� �������� ������� 1, ������� �������-���� ������� 2: ";
        int input = 0;
        cin >> input;
        switch(input) {
            case 1:
                this->coding_type = 1;
                error = false;
                break;
            case 2:
                this->coding_type = 2;
                error = false;
                break;
            default:
                cout << "�������� ����!" << endl;
                break;
        }
    }
}

/**
 * ������ � ������������ ������������ ���������� ��������� ��� ���������
 * @return ���������� ��������, ������� ���������� �������������
 */
int Input::askGeneratorCharNum() {
    cin.clear();
    cout << "������� ���������� �������� ��� ���������: ";
    int n = 0;
    bool error = true;
    while(error) {
        cin >> n;
        if(!cin) { //���� ���� ������� �� �����, �� ������
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        } else {
            error = false;
        }
    }
    return n;
}

/**
 * ������ ������ � ����������� �� ���������� ������������� ���� �����
 * @return ��������� ��� ��������������� �����
 */
string Input::getInput() {
    string text;
    switch(this->input_type) { //������ ��������� ������ � ����������� �� ���� �����
        case 1:
            cout << "������� �����: " << endl; //������ � �������
            getchar();
            cin >> text;
            break;
        case 2:
            File::read(text); //������ �� �����
            break;
        case 3:
            this->askGeneratorType();
            text = this->getDistribution(); //���������

    }
    return text;
}

/**
 * ������������� ��������������� ������ ��������� � ����������� �� ���������� ������������� ����
 * @param distributor - ������������� ������ ���������� �������� � �������� ������
 * @return @class Huffman ��� @class ShannonFano
 */
Coding* Input::getCoding(Distributor* distributor) {
    if(this->coding_type == 1) { //���� ��� 1 - �� �������������� ������ ��������
        return new Huffman(distributor->formElemDistribution());
    } else if(this->coding_type == 2) { //���� 2 - �� ������ �������-����
        return new ShannonFano(distributor->formElemDistribution());
    }
}

/**
 * ������������ ������ � �������������� ����� ��������� � ����������� �� ���������� ������������� ����
 * @return ������ � �������
 */
string Input::getDistribution() {
    if(this->generator_type == 1) { //���� ��� 1 - �� ����������� �������������
        return Generator::uniformDistribution(this->askGeneratorCharNum());
    } else if(this->generator_type == 2) { //���� 2 - �� �������������
        return Generator::randomDistribution(this->askGeneratorCharNum());
    }
}