#include <sstream>
#include "Decoder.h"

/**
 * ������������� ��������� ������
 * @param head - ��������� �� ������� ������
 * @param decoded - ������ �� ������ ��� ������ ��������������� ������
 */
void Decoder::decode(TreeElement* head, string& decoded) {
    stringstream stream; //����� ��� ������ ��������������� ���������
    char symb;
    string route;
    //���� � ������ ������ ���� �������, �� ���������� �������� ��� �������
    //��������������� ��������� �� ������ ����� ��������
    if(head->getLeft() == nullptr && head->getRight() == nullptr) {
        while(true) {
            stream << head->getData();
            if(!Text::moveToNext(current_input)) {
                break;
            }
        }
    } else { //�� ���� ��������� �������
        while(getDecodedSymbol(head, &symb, route)) {
            stream << symb; //���� � �������������� ��������� �������� �������
            //�������� �� �� ������ ����
        }
    }
    decoded = stream.str(); //��������� ������ �� ������ � ������
}

/**
 * ������������� �������� �������
 * @param current - ��������� �� ������� ������� ������
 * @param symb - ��������� �� ������, ������� ����������
 * @param route - ��� �������
 * @param skip - ������� ����������
 * @return �������������� ������
 */
bool Decoder::getDecodedSymbol(TreeElement* current, char* symb, string route, bool skip) {
    if(skip) Text::moveToNext(current_input); //��� ����������� ������� ��������� ������� �� ��������� ������
    if(current->getLeft() != nullptr && current->getRight() != nullptr) { //���� ������� ���� �� ����
        if(Text::getCurrentSymbol(current_input) == '0') {
            return this->getDecodedSymbol(current->getLeft(), symb, route + '0', true);
        } else if(Text::getCurrentSymbol(current_input) == '1') {
            return this->getDecodedSymbol(current->getRight(), symb, route + '1', true);
        }
    } else { //�����, ��� ��� ������ ������� �������� ������, �� ���������� ��� ������ � �������������� ������
        return (*symb = current->getData());
    }
}