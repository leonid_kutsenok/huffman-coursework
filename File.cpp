#include "File.h"

/**
 * ������ �� �����
 * @param text - ������ �� ������ ��� ������ ��������� ������
 * @return ��������� ���������� ������
 */
bool File::read(string& text) {
    ifstream fin("in.txt");
    if(!fin.is_open()) {
        cout << "������ ������ �� �����";
        return false;
    }
    text = static_cast<stringstream const&>(stringstream() << fin.rdbuf()).str();
    return true;
}

/**
 * ������ � ����
 * @param text - ������ �� ������ ��� ������ ������
 * @return
 */
bool File::writeText(string& text) {
    ofstream fout("out.txt");
    fout << text;
    fout.close();
    return true;
}

bool File::writeCode(string& text) {
    ofstream fout("code.txt");
    fout << text;
    fout.close();
    return true;
}
