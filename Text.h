#ifndef KURSACH_TEXT_H
#define KURSACH_TEXT_H

#include <iostream>
#include <string>
#include <cmath>

using namespace std;

class Text {
public:
    static int getByteLength(string& encoded);
    static bool moveToNext(char*& current_input);
    static char getCurrentSymbol(char* current_input);
};


#endif //KURSACH_TEXT_H
