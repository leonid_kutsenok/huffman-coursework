#include <iostream>
#include "Distributor.h"
#include "File.h"
#include "Encoder.h"
#include "Decoder.h"
#include "Input.h"
#include "Timer.h"

int main() {
    setlocale(LC_CTYPE, ".1251");
    auto* timer = new Timer();
    auto* input = new Input();
    input->askInputType(); //����� ���� ����� (�������/����/���������)
    string text = input->getInput(); //������ ��������� ������
    if(text.length() == 0) { //���� ��������� ������ ���, ������� ������
        cout << "������! ������ ����";
        return 0;
    }
    stringstream output; //����� ��� ������ ��������� � ����
    output << "�������� �����:" << endl << text << endl;
    auto* distributor = new Distributor();
    distributor->formDistribution(text); //������������� �������� �� �������� �� ���������� � ������

    input->askCodingType(); //����� ���� �����������

    Coding* coding = input->getCoding(distributor); //�������� ������ ���������

    timer->start();
    coding->formTree(); //������������ ������ �����������
    cout << "�� ������������ ������ ���������� " << timer->stop() << " ��" <<endl;

    auto* encoder = new Encoder(text);
    string encoded;
    timer->start();
    encoder->encode(coding->getTree()->getHead(), encoded); //����������� ��������� ���������
    output << "�������������� �����:" << endl << encoded << endl;
    cout << "�� ����������� ��������� ������ ��������� " << timer->stop() << " ��" << endl;

    auto* decoder = new Decoder(encoded);
    string decoded;
    timer->start();
    decoder->decode(coding->getTree()->getHead(), decoded); //������������� ��������� ���������
    output << "��������������� �����" << endl << decoded << endl;
    string out = output.str();
    File::writeText(out); //���������� ����� � ����
    cout << "�� ������������� ��������� ������ ��������� " << timer->stop() << " ��" << endl;
    cout << "���������� ����� � ������ �����������: " << coding->getTree()->getNodesCount() << endl;
    cout << "����� ��������� ��������� (� ������): " << text.length() << endl;
    cout << "����� ��������������� ��������� (� ������): " << Text::getByteLength(encoded) << endl;
    cout << coding->counter << endl;
    return 0;
}